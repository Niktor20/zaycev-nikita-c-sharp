﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildHouse
{
    public class Manager
    {
        private House M_house = new House();
        public Manager(Builder builder)
        {
            M_house = builder._house;
            Console.Write("Manager ");
            builder.SetFloor();
            Console.Clear();
            Console.Write("Manager ");
            builder.SetWindow();
            Console.Clear();
            Console.Write("Manager ");
            builder.SetRoof();
            Console.Clear();
        }

        public void ShowHouse()
        {
            Console.WriteLine("Manager ");
            Console.WriteLine(M_house.material);
            Console.WriteLine("Тип фундамента - "+M_house.TypeOfFoundation);
            Console.WriteLine("Количество окон: "+M_house.CountOfWindow);
            Console.WriteLine("Тип крыши - "+M_house.TypeOfRoof);
            Console.WriteLine("Количество этаже: "+ M_house.CountOfFloor);
        }

    }
}
