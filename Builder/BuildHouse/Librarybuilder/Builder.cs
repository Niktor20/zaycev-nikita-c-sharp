﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildHouse
{
   public  abstract class Builder
    {

        public  int a = 0;
        public House _house =new House();

        public  Builder(House house)
        {
            _house=house;
        }


        virtual public int SetFloor() 
        {
            
            Console.WriteLine("введите количество этажей в вашем доме");
            _house.CountOfFloor= Convert.ToInt32(Console.ReadLine());
             
            return _house.CountOfFloor;
            Console.Clear();
        }

        virtual  public int SetWindow()
        {
            
            Console.WriteLine("введите количество окон в вашем доме");
            _house.CountOfWindow = Convert.ToInt32(Console.ReadLine());
       
            return (_house.CountOfWindow);
            Console.Clear();

        }

        virtual public string SetRoof ()
        {
            Console.Clear();
            while (this._house.TypeOfRoof != "1"&& this._house.TypeOfRoof != "2")
            {
                Console.WriteLine("выберите тип крыши (1 или 2 )");
                Console.WriteLine("1 == Односкатная, 2== Двухсткатная");
                this._house.TypeOfRoof = Console.ReadLine();

                if (this._house.TypeOfRoof != "1" && this._house.TypeOfRoof != "2")
                {
                    Console.Write("Выберите из предложенных вариантов, мы не строем других крыш.");
                }

            }
            Console.Clear();
            if (this._house.TypeOfRoof == "1")
            {
                this._house.TypeOfRoof = "Односкатная";
            }
            else { this._house.TypeOfRoof = "Двухсткатная"; }
            Console.WriteLine("Тип крыши " + this._house.TypeOfRoof);
            return this._house.TypeOfRoof;
            
        



        }
        virtual public void ShowHome()
        {
            Console.WriteLine(_house.material);
            Console.WriteLine("Тип фундамента - " + _house.TypeOfFoundation);
            Console.WriteLine("Количество окон: " + _house.CountOfWindow);
            Console.WriteLine("Тип крыши - " + _house.TypeOfRoof);
            Console.WriteLine("Количество этаже: " + _house.CountOfFloor);

        }


    }

    public class WoodBuilder: Builder
     {

        public WoodBuilder(House house) : base(house)
        {
            _house = house;
            _house.TypeOfFoundation = "Винтовой";
            _house.material = "Дом из дерева";
           
        }
       
     }

    public class MonolithBuilder : Builder
    {

        public MonolithBuilder(House house) : base(house)
        {
            _house = house;
            house.TypeOfFoundation = "Плита";
            _house.material = "Монолитный котедж";
        
        }
       
      
    }

    public class SipPanelBuilder : Builder
    {
        public SipPanelBuilder(House house) : base(house)
        {
            _house = house;
            house.TypeOfFoundation = "Винтовой";
            house.material = "Дом из СипПанелей";
           
        }
       
    }

}
