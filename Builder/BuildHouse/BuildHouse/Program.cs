﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildHouse
{
    internal class Program
    {
        static void Main(string[] args)
        {
            House house = new House();                /* Тут реализованно созоздание дома через строителя, когда клиент пошагово выполняет возведение дома */
            Builder builder = new WoodBuilder(house);
            builder.SetRoof();
            builder.SetFloor();
            builder.SetWindow();
            builder.ShowHome(); /*Вывод полей  дома в консоль*/

            Console.WriteLine();

            House house2 = new House();              
            Builder builder2 = new MonolithBuilder(house);


            Manager manager = new Manager(builder2); /*В момент передачи в конструктор манагера  строителя, манагер поочериди выполняет все операции
                                                      * необходимые для окончательного возведения дома*/

            manager.ShowHouse();   /*Вывод полей  дома в консоль*/

        }
    }
}
