﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Тут  реализуется интерфейс трёх фабрик: 1.Фабрики производящей МодернМебель
 *                                         2.Фабрики производящей АртДекоМебель
 *                                         3.Фабрики производящей ВикториМебель*/

namespace AbstractFacrory
{
    public interface IFabric
    {
        IChair GetChair(string color);
        ISofa GetSofa(string color);
        ITable GetTable(string color);

    }

    public class FabricModern : IFabric
    {
        private string m_colorChair;
        private string m_colorSofa;
        private string m_colorTable;

       public IChair GetChair(string color) 
        {
            m_colorChair = color;
            var chair = new ModernChair(m_colorChair);
            Console.WriteLine("MODERN Кресло " + this.m_colorChair);
            return chair;
        }
       public ISofa GetSofa(string color)
        {
            m_colorSofa = color;
            var sofa = new ModernSofa(m_colorSofa);
            Console.WriteLine("MODERN Диван " + this.m_colorSofa);
            return sofa;
        }
        public ITable GetTable(string color)
        {
            m_colorSofa = color;
            ITable table;
            table = new ModernTable(m_colorTable);
            Console.WriteLine("MODERN Столик " + this.m_colorTable);
            return table;

        }


    }

    public class FabricVictorian: IFabric
    {
        private string m_colorChair;
        private string m_colorSofa;
        private string m_colorTable;

        public IChair GetChair(string color)
        {
            m_colorChair = color;
            var chair = new VictoryanChair(m_colorChair);
            Console.WriteLine("VICT Кресло " + this.m_colorChair);
            return chair;
        }
        public ISofa GetSofa(string color)
        {
            m_colorSofa = color;
            var sofa = new VictoryanSofa(m_colorSofa);
            Console.WriteLine("VICT Диван " + this.m_colorSofa);
            return sofa;
        }
        public ITable GetTable(string color)
        {
            m_colorSofa = color;
            ITable table;
            table = new VictoryanTable(m_colorTable);
            Console.WriteLine("VICT Столик " + this.m_colorTable);
            return table;

        }


    }
    public class FabricArtDeco : IFabric
    {
        private string m_colorChair;
        private string m_colorSofa;
        private string m_colorTable;

        public IChair GetChair(string color)
        {
            m_colorChair = color;
            var chair = new ArtChair(m_colorChair);
            Console.WriteLine("ART Кресло " + this.m_colorChair);
            return chair;
        }
        public ISofa GetSofa(string color)
        {
            m_colorSofa = color;
            var sofa = new ArtSofa(m_colorSofa);
            Console.WriteLine("ART Диван " + this.m_colorSofa);
            return sofa;
        }
        public ITable GetTable(string color)
        {
            m_colorSofa = color;
            var table = new ArtTable(m_colorTable);
            Console.WriteLine("ART Столик " + this.m_colorTable);
            return table;
            

        }


    }











}
