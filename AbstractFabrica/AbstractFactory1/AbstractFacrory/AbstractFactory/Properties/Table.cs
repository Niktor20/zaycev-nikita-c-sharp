﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/* Тут различные  виды конкретных продуктов  реализуют свой интерфейс */

namespace AbstractFacrory
{
    public interface ITable
    {
        string convenience();
        string legs();
        string view();
        string shelves();
        string size();



    }

    public  class ModernTable : ITable
    {
        public const string Name = "Modern";

        public string ProductOfFurniture;
        public ModernTable(string Name) { this.ProductOfFurniture = Name; }
        public string convenience() { return "Очень удобный"; }
        public string legs() { return "Ножек нет"; }
        public string shelves() { return "Нет полочек"; }
        public string size(){ return "Средний"; }
        public string view() { return "Необычный"; }


    }

    public  class VictoryanTable : ITable
    {
        public const string Name = "Victoryan";

        public string ProductOfFurniture;
        public VictoryanTable(string Name) {this.ProductOfFurniture = Name;}
        public string convenience() { return "Очень неудобный";}
        public string legs() { return "Четыре";}
        public string shelves() { return "Одна полочка";}
        public string size() { return "Маленький";}
        public string view() { return "Дорогой"; }

    }

    public class ArtTable : ITable
    {
        public const string Name = "ArtDeco";

        public string ProductOfFurniture;
        public ArtTable(string Name) { this.ProductOfFurniture = Name; }
        public string convenience() { return "Удобное"; }
        public string legs() { return "Нет ножек"; }
        public string shelves() { return "Нет полочек"; }
        public string size() { return "Большой"; }
        public string view() { return "Обычный"; }

    }
}
