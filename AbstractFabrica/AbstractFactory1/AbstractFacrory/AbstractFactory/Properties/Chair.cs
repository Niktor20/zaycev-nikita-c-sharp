﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



/* Тут  различные  виды конкретных продуктов  реализуют свой интерфейс */

namespace AbstractFacrory
{
    public interface IChair
    {
         string backrest();
         string softness();
         string convenience();
         string legs();
         string view();

    }

   public class ModernChair : IChair
    {
        public const string Name = "Modern";

        public string ProductOfFurniture;

        public ModernChair(string Name) { this.ProductOfFurniture = Name; }
        public string backrest() { return "Закруглённая спинка"; }
        public string softness() { return "Очень твёрдое"; }
        public string convenience() { return "Очень удобное"; }
        public string legs() { return "Ножек нет"; }
        public string view() { return "Необычный"; }

    }

    public class VictoryanChair : IChair
    {
        public const string Name = "Victoryan";

        public string ProductOfFurniture;
        public VictoryanChair(string Name) { this.ProductOfFurniture = Name; }

        public string backrest() { return "Ровная спинка"; }
        public string softness() { return "мягкое"; }
        public string convenience() { return "Очень неудобное"; }
        public string legs() { return "Четыре"; }
        public string view() { return "Дорогой"; }

    }

    public class ArtChair : IChair
    {
        public const string Name = "ArtDeco";

        public string ProductOfFurniture;
        public ArtChair(string Name) { this.ProductOfFurniture = Name; }
        public string backrest() { return "Большая спинка"; }
        public string softness() { return "Очень мягкое"; }
        public string convenience() { return "удобное"; }
        public string legs() { return "Нет ножек"; }
        public string view() { return "Обычный"; }

    }
}
