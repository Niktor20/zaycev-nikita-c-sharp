﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Тут  различные  виды конкретных продуктов  реализуют свой интерфейс */
namespace AbstractFacrory
{
    public interface ISofa
    {
        string Size();
        string softness();
        string convenience();
        string legs();
        string view();
        string modifications();

    }

    public class ModernSofa : ISofa
    {
        public const string Name = "Modern";

        public string ProductOfFurniture;
        public ModernSofa(string Name) { this.ProductOfFurniture = Name; }
        public string Size() { return "Средний"; }
        public string softness() { return "Очень твёрдое"; }
        public string convenience() { return "Очень удобное"; }
        public string legs() { return "Ножек нет"; }
        public string view() { return "Необычный"; }
        public string modifications(){return "Нераскладной";}

    }

    public  class VictoryanSofa : ISofa
    { 

        public const string Style = "Victoryan";

        public string ProductOfFurniture;
        public VictoryanSofa(string Name) { this.ProductOfFurniture = Name; }
        public  string Size() { return "Маленький"; }
        public string softness() { return "мягкое"; }
        public string convenience() { return "Очень неудобное"; }
        public string legs() { return "Четыре"; }
        public string view() { return "Дорогой"; }
        public string modifications() { return "Нераскладной"; }

    }

    public class ArtSofa : ISofa
{
        public const string Name = "ArtDeco";

        public string ProductOfFurniture;
        public ArtSofa(string Name) { this.ProductOfFurniture = Name; }
        public string Size() { return "Большой"; }
        public string softness() { return "Очень мягкое"; }
        public string convenience() { return "удобное"; }
        public string legs() { return "Нет ножек"; }
        public string view() { return "Обычный"; }
        public string modifications() { return "Раскладной"; }

    }
}

