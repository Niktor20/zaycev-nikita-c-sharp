﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractFacrory;

namespace AbstractCons
{
    public class AProgramm
    {
        static void Main(string[] args)
        {
            IFabric MODERN = new FabricModern();
            IFabric VICTORIAN = new FabricVictorian();
            IFabric ARTDECO = new FabricArtDeco();



            MODERN.GetSofa("Зелённый");  /*в конструкторе определяется цвет нужного продукта, таким образом реализуется возможность
                                           фабрики производить разные товары в рамках одного вида.  
                                        Метод GetSofa возвращает объект с его характерными методами и свойствами.

                                         ВОПРОС: Нужно ли чтобы возвращаемые свойства распечатывались в консоли? 
                                              Так как  метод класса возвращает их через return:
                                        */
            MODERN.GetTable("Зелённый");
            MODERN.GetChair("Зелённый");


            VICTORIAN.GetSofa("Золотой");
            VICTORIAN.GetTable("Золтой");
            VICTORIAN.GetChair("Золотой");


            ARTDECO.GetSofa("Красное");
            ARTDECO.GetTable("Красное");
            ARTDECO.GetChair("Красное");




        }
    }
}
